import math

class kmeans:
    def __init__(self):
        self.clusters = 3
        self.reassign = False
        self.datapoints = []
        self.centroid1 = [-112,33]
        self.centroid2 = [-80,43]
        self.centroid3 = [-80,35]

    def euclidean_distance(self, x, y, a, b):
        distance = math.sqrt(((b-y)*(b-y))+((a-x)*(a-x)))
        return distance

    def cluster(self, i):
        location = self.datapoints[i]
        d1 = self.euclidean_distance(self.centroid1[0], self.centroid1[1], location[0], location[1])
        d2 = self.euclidean_distance(self.centroid2[0], self.centroid2[1], location[0], location[1])
        d3 = self.euclidean_distance(self.centroid3[0], self.centroid3[1], location[0], location[1])
        #While clustering check if cluster is changed then set reassign to True
        if d1<d2:
            if d1<d3:
                if location[2] != 0:
                    self.reassign = True
                location[2] = 0
            else:
                if location[2] != 2:
                    self.reassign = True
                location[2] = 2
        else:
            if d2<d3:
                if location[2] != 1:
                    self.reassign = True
                location[2] = 1
            else:
                if location[2] != 2:
                    self.reassign = True
                location[2] = 2
        self.datapoints[i] = location

    def recompute_centers(self):
        new_centroid1 = [0,0]
        new_centroid2 = [0,0]
        new_centroid3 = [0,0]
        pointsC1 = 0
        pointsC2 = 0
        pointsC3 = 0
        for i in range(len(self.datapoints)):
            values = self.datapoints[i]
            if values[2] == 0:
                new_centroid1[0] = new_centroid1[0] + float(values[0])
                new_centroid1[1] = new_centroid1[1] + float(values[1])
                pointsC1 = pointsC1 + 1
            if values[2]:
                new_centroid2[0] = new_centroid2[0] + float(values[0])
                new_centroid2[1] = new_centroid2[1] + float(values[1])
                pointsC2 = pointsC2 + 1
            if values[2] == 2:
                new_centroid3[0] = new_centroid3[0] + float(values[0])
                new_centroid3[1] = new_centroid3[1] + float(values[1])
                pointsC3 = pointsC3 + 1
        new_centroid1[0] = new_centroid1[0] / pointsC1
        new_centroid1[1] = new_centroid1[1] / pointsC1
        new_centroid2[0] = new_centroid2[0] / pointsC2
        new_centroid2[1] = new_centroid2[1] / pointsC2
        new_centroid3[0] = new_centroid3[0] / pointsC3
        new_centroid3[1] = new_centroid3[1] / pointsC3


if __name__ == '__main__':

    my_kmeans = kmeans()
    print("Going to start kmeans algoritgm")

    # Step 1- Read places.txt and extract all points into a datapoints dictionary
    file = open("places.txt","r")
    x = 0
    for line in file:
        x = x +1
        values = line.rstrip().split(',')
        my_kmeans.datapoints.append([float(values[0]), float(values[1]), 0])
    # Step 2- Assign centroids for the 3 clusters

    # Step 3- Write function to compute euclidean distance

    #Step 4- Compute euclidean distance for each point from all 3 centres and insert in data points dictionary
    my_kmeans.reassign = True
    while my_kmeans.reassign == True:
        my_kmeans.reassign = False
        for i in range(len(my_kmeans.datapoints)):
            my_kmeans.cluster(i)
        #Step 5- Recompute centers till reassign stays false
        if my_kmeans.reassign == True:
            my_kmeans.recompute_centers()
        else:
            break
    print("Finished clustering")


    #Step 6- Output data into clusters.txt
    file = open("clusters.txt","w")
    i = 0
    for i in range(len(my_kmeans.datapoints)):
        value = my_kmeans.datapoints[i]
        file.write("%d %d\n" % (i, value[2]))
        i = i + 1
    file.close()